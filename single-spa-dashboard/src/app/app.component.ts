import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  route(url: string) {
    window.history.pushState(null, null, url);
  }

  barChart: SafeResourceUrl;
  lineChart: SafeResourceUrl;
  pieChart: SafeResourceUrl;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
    const barchart = 'http://10.10.130.68:8503/dashboard/assets/barchart.png';
    const linechart = 'http://10.10.130.68:8503/dashboard/assets/line-chart.png';
    const piechart = 'http://10.10.130.68:8503/dashboard/assets/pie-chart.png';
    this.barChart = this.sanitizer.bypassSecurityTrustResourceUrl(barchart);
    this.lineChart = this.sanitizer.bypassSecurityTrustResourceUrl(linechart);
    this.pieChart = this.sanitizer.bypassSecurityTrustResourceUrl(piechart);
  }
}
